import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './App.css';

import _ from 'lodash'
import PostsList from './Containers/PostsList';
import Post from './Containers/Post';

class App extends React.Component {

  render() {
    return (
      <Switch>
        <Route exact={true} path='/' component={PostsList} />
        <Route path='/posts/:postId' component={Post} />
      </Switch>
    );
  }
}


export default App;
