import React from 'react';
import { connect } from 'react-redux';
import { store } from './Store'
import '../App.css';
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { getAllPosts } from './Actions';

class PostsList extends React.Component {

    componentDidMount() {
        store.dispatch(getAllPosts())
    }

    renderDate = () => {
        if (!this.props) { return null }
        return _.map(this.props.state.posts, ({ data }) => _.map(data, ({ id, title, body, creator, date }) => {
            return <Link to={'/posts/' + id} className="post">
                <div className='postTitle'>{title}</div>
                <div className='postBody'>{body}</div>
                <div className='postFooter'>
                    <div className="postAuchor">Автор: {creator}</div>
                    <div className="postDate">Дата: {date}</div>
                </div>
            </Link>
        }))
    }
    render() {
        return (
            <div className="App">
                {this.renderDate()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { state }
}

export default connect(mapStateToProps)(PostsList);
