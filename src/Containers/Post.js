import React from 'react'
import _ from 'lodash'

import { connect } from 'react-redux';
import { store } from './Store'
import { getCurrentPost } from './Actions';

import './Post.css'

class Post extends React.Component{

    componentDidMount(){
        store.dispatch(getCurrentPost(this.props.match.params.postId))
    }

    renderPostInfo = () => {
        if(!this.props.state.currentPost.postInfo) {return null}
        const renderComments = _.map(this.props.state.currentPost.postInfo.data.comments, ({body}) => {
            return <div className="comments">Comment: {body}</div>
        })
        return <div>
            <div>{this.props.state.currentPost.postInfo.data.title}</div>
            <div>{this.props.state.currentPost.postInfo.data.body}</div>
            {renderComments}
        </div>
    }

    render(){
        return(
            <div>{this.renderPostInfo()}</div>
        )
    }
}

const mapStateToProps = (state) => {
    return { state }
}

export default connect(mapStateToProps)(Post)