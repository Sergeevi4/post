import Axios from 'axios';

export const getAllPosts = () => dispatch => {
    setTimeout(() => {
        Axios.get('https://simple-blog-api.crew.red/posts').then((res) => {
            dispatch({ type: 'GET_ALL_POSTS', payload: res })
        })
    }, 1000)
}

export const getCurrentPost = (id) => dispatch => {
    setTimeout(() => {
        Axios.get('https://simple-blog-api.crew.red/posts/'+ id + '?_embed=comments').then((res) => {
        dispatch({type: 'GET_CURRENT_POST', payload: res})
    })
    }, 1000)
}