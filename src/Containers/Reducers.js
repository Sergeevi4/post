import { combineReducers } from 'redux'

function posts(state = [], action) {
    switch (action.type) {
        case 'GET_ALL_POSTS':
            return [
                action.payload
            ]
        default:
            return state;
    }
}

function currentPost(state = {}, action) {
    switch (action.type) {
        case 'GET_CURRENT_POST':
            return {
                ...state,
                postInfo: action.payload
            }
        default:
            return state;
    }
}

export default combineReducers({
    posts,
    currentPost
})